const q = e => {

        return document.querySelector( e );
    
    }, qAll = e => {
    
        return document.querySelectorAll( e );
    
    };

const calcMultiple = ( marquee, pWidth ) => {

    let width = marquee.offsetWidth;

    return {

        width: () => width,
        multiple: () => Math.round( width / pWidth ),
        difference: () => Math.abs( width - pWidth ),

    } 

}, setNew = ( sentence, container, left ) => {

    let clone = sentence.cloneNode( true );
        clone.style.transform = `translate(${left}px)`;
    
    container.appendChild( clone );

}, setMarquee = ( marquee, pWidth ) => {

    let multiple = calcMultiple( marquee, pWidth ).multiple(),
        left = 0,
        sentences = qAll( "#marquee > div p" ),
        sentencesLength = sentences.length - 1;

    for ( let i = 1; i <= multiple; i++ ) {
    
        left = left + pWidth;

        if ( i > sentencesLength ) {

            console.log( "new sentence" );
        
            setNew( left );
            
        }
    
    }

}, removeFirst = () => {

    let first = q( "#marquee > div p:first-child" );
        first.parentNode.removeChild( first );

}, launchMarquee = ( marquee, container, sentence, pWidth, pHeight ) => {

    let count = 0,
        interval_id = window.setInterval("", 9999);
    for ( let i = 1; i < interval_id; i++ ) window.clearInterval(i);

    pWidth = sentence.offsetWidth;

    setInterval( () => {
    
        let sentences = qAll( "#marquee > div p" );
        
        for ( let i = 0; i <= sentences.length - 1; i++ ) {
        
            let left = pWidth * i,
                pos = left - count,
                padding = i * pHeight * 0.4;
            
                sentences[ i ].style.transform = `translate( ${ pos + padding }px )`;

        }

        if ( count === calcMultiple( marquee, pWidth ).difference() ) setNew( sentence, container, pWidth );
            
        if ( count === pWidth ) { count = 0; removeFirst(); } else { count += 1 }
    
    }, 10 );

}, initMarquee = () => {

    const marquee = q( "#marquee" );
    const container = q( "#marquee > div" );
    const sentence = q( "#marquee > div p" );

    let pWidth = sentence.offsetWidth;
    let pHeight = sentence.offsetHeight;

    setMarquee( marquee, pWidth );
    launchMarquee( marquee, container, sentence, pWidth, pHeight );

};

export {

    calcMultiple,
    setNew,
    setMarquee,
    removeFirst,
    launchMarquee,
    initMarquee,

};