"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setLeft = exports.setZ = exports.setOrder = exports.sortId = exports.initOrder = void 0;

var initOrder = function initOrder(ul) {
  var lis = ul.querySelectorAll("li");

  for (var i = 0; i < lis.length; i++) {
    lis[i].dataset.id = i;
  }
},
    sortId = function sortId(ul) {
  return [].map.call(ul.querySelectorAll("li"), function (el) {
    return el;
  }).sort(function (a, b) {
    var filter_a = parseInt(a.dataset.id),
        filter_b = parseInt(b.dataset.id);
    return filter_a < filter_b ? -1 : filter_a > filter_b ? 1 : 0;
  });
},
    setOrder = function setOrder(e, ul) {
  var list = sortId(ul);

  for (var i = 0; i < e.dataset.id; i++) {
    list[i].dataset.id = i + 1;
  }

  e.dataset.id = 0;
  setZ(ul);
  setLeft(ul);
},
    setZ = function setZ(ul) {
  var lis = ul.querySelectorAll("li");

  for (var i = lis.length; i > 0; i--) {
    sortId(ul)[i - 1].style.zIndex = i;
  }
},
    setLeft = function setLeft(ul) {
  var lis = ul.querySelectorAll("li");

  for (var i = 0; i < lis.length; i++) {
    var left = 0;

    for (var j = 0; j < i; j++) {
      left = left + sortId(ul)[j].offsetWidth;
    }

    sortId(ul)[i].style.left = "".concat(left, "px");
  }
};

exports.setLeft = setLeft;
exports.setZ = setZ;
exports.setOrder = setOrder;
exports.sortId = sortId;
exports.initOrder = initOrder;